"""
Code to find position of number in Fibonacci series

To run: python3 fibonacci.py 21
"""

import sys


class Error(Exception):
    """Base class for other exceptions"""
    pass


class ValueExactError(Error):
    """Raised when the input is greater or less than one"""
    pass


class ValueNotInteger(Error):
    """Raised when the input is not integer"""
    pass


class NoInputError(Error):
    """Raised when no input is provided"""
    pass


class Fibonacci:
    """Print the position of number in Fibonacci series"""

    def __init__(self, number):
        self.number = number

    def series(self):

        num1, num2 = 0, 1
        count = 0
        pos = 1

        try:
            if len(self.number) == 0:
                raise NoInputError

            if len(self.number) < 1 or len(self.number) > 1:
                raise ValueExactError

            # to convert list to interger
            strings = [str(integer) for integer in self.number]
            a_string = "".join(strings)

            if a_string.isnumeric() == False:
                raise ValueNotInteger

            an_integer = int(a_string)

            while count < pos:
                # check if provided number is equal to number in series
                if num1 == an_integer:
                    print("position is {}".format(pos))
                    break

                # if number is not present in series, so the loop break beacause the 
                # number exceeds the series
                elif num1 > an_integer:
                    print(-1)
                    break
                nth = num1 + num2
                # update values
                num1 = num2
                num2 = nth
                count += 1
                pos += 1

        except ValueExactError:
            print("provide only one value")
        except ValueNotInteger:
            print("Provide only integer")
        except NoInputError:
            print("No input")


Fibonacci(sys.argv[1:]).series()
