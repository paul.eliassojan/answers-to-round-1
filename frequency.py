"""
Code to find the largest occurance of a word in the sentence

To run: python3 frequency.py "random words paper place words butterfly compiler then there words"
"""

import sys


class Error(Exception):
    """Base class for other exceptions"""
    pass


class NoInputError(Error):
    """Raised when no input is provided"""
    pass


class Frequency:
    """ To Print the largest occuring word"""

    def __init__(self):
        pass

    def word(self, sentence):
        maxCount = 0
        word = ""
        try:
            if len(sentence) == 0:
                raise NoInputError

            txt = sentence[0].split()
            for i in range(0, len(txt)):
                count = 1
                # Count each word in the list and store it in variable count
                for j in range(i+1, len(txt)):
                    if(txt[i] == txt[j]):
                        count = count + 1

                # If maxCount is less than count then store value of count in maxCount
                # and corresponding word to variable word
                if(count > maxCount and count > 1):
                    maxCount = count
                    word = txt[i]

            if len(word) != 0:  # if repetition words exist

                print("Most repeated word: {}".format(word))

            else:
                print("no repeated words found")

        except NoInputError:
            print("No input")


Frequency().word(sys.argv[1:])
