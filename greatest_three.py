"""
Code to find greatest of 3 numbers from command line argument

To run: python3 greatest_three.py 20 30 1
"""

import sys


class Error(Exception):
    """Base class for other exceptions"""
    pass


class ValueExactError(Error):
    """Raised when the input is less than or greater than three"""
    pass


class ValueNotInteger(Error):
    """Raised when the input is not integer"""
    pass


class NoInputError(Error):
    """Raised when no input is provided"""
    pass


class Greatest_three:
    """Print the greatest of three numbers"""

    def __init__(self, big):
        self.big = big

    def find(self):
        try:
            if len(self.big) == 0:
                raise NoInputError

            if len(self.big) < 3 or len(self.big) > 3:  # check if length of value greater than 3
                raise ValueExactError

            maximum = self.big[0]
            for i in self.big:
                if i.isdigit() == False:  # check if value is not digit
                    raise ValueNotInteger
                if int(i) > int(maximum):  # to find maximum value
                    maximum = i
            print("Biggest number is {}".format(maximum))
        except ValueExactError:
            print("enter exactly three numbers")
        except ValueNotInteger:
            print("enter only integers")
        except NoInputError:
            print("No input")


Greatest_three(sys.argv[1:]).find()
